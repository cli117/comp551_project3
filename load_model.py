from keras.models import load_model
import pandas
import numpy as np
import cv2
import keras

img_rows = 128
img_cols = 128

x_test = pandas.read_pickle('test_max_x')

x_test_rgb = np.zeros((x_test.shape[0], img_rows, img_cols, 3))
i = 0
for img in x_test:
    x_test_rgb[i] = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    i = i + 1
x_test_rgb = keras.applications.vgg16.preprocess_input(x_test_rgb)
model = load_model('model.h5')

predictions = model.predict(x_test_rgb)
results = []
for i in range(len(predictions)):
    results.append(np.where(predictions[i] == np.amax(predictions[i])))
    results[i] = (i, results[i][0][0])

prediction = pandas.DataFrame(results, columns=['Id', 'Label']).to_csv('prediction_128_aug.csv', index=False)



