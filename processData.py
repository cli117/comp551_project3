import numpy as np
import pandas as pd
from PIL import Image



df = pd.read_csv('train_max_y.csv')

for i in range(len(df)):
    df['Id'].iloc[i] = str(i) + ".png"

df.to_csv('train_map.csv', index=False)

