from __future__ import print_function
import keras
from keras.callbacks import EarlyStopping
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
import pandas
import numpy as np
from keras.applications.vgg16 import VGG16
from keras_preprocessing.image import ImageDataGenerator

batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 128, 128

# the data, split between train and test sets
#(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train = pandas.read_pickle('train_max_x')
x_test = pandas.read_pickle('test_max_x')

print('x_test shape:',x_test.shape)
# Binarize
# for i, img in enumerate(x_train): x_train[i] = cv2.threshold(img, 225, 255, cv2.THRESH_BINARY)


x_validation = x_train

y_train = pandas.read_csv('train_max_y.csv',header=0)['Label'].values
y_validation = y_train


if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_validation = x_validation.reshape(x_validation.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_validation = x_validation.reshape(x_validation.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_validation = x_validation.astype('float32')
x_train /= 255
x_validation /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_validation.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_validation = keras.utils.to_categorical(y_validation, num_classes)

print('y_train shape:', y_train.shape)
print(y_train.shape[0], 'train samples')
print(y_validation.shape[0], 'test samples')
print(y_train)

# image augmentation
datagen = ImageDataGenerator(rotation_range=45, width_shift_range=0.1, height_shift_range=0.1, shear_range=0.2)
it = datagen.flow(x_train, y_train, batch_size=1)

model = keras.Sequential()

model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))


model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

# Early stopping
es = EarlyStopping(monitor = 'val_loss', mode = 'min', verbose = 1)
model.fit_generator(it, steps_per_epoch=391)
model.fit(x_train, y_train, batch_size=batch_size, epochs=15, verbose=1, callbacks=[es])

score = model.evaluate(x_validation, y_validation, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

model.save('model_aug.h5')


