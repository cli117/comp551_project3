from __future__ import print_function
import keras

from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Flatten
import pandas
import numpy as np
from keras.applications.vgg16 import VGG16
import matplotlib.pyplot as plt
import cv2
import pickle

from keras_preprocessing.image import ImageDataGenerator

num_classes = 10
batch_size = 128
epochs = 12

# input image dimensions
img_rows, img_cols = 128, 128

x_dataset = pandas.read_pickle('train_max_x')
y_dataset = pandas.read_csv('train_max_y.csv', header=0)['Label'].values

x_dataset_rgb = np.zeros((x_dataset.shape[0], img_rows, img_cols, 3))
i = 0
for img in x_dataset:
    x_dataset_rgb[i] = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    i = i + 1
x_dataset_rgb = keras.applications.vgg16.preprocess_input(x_dataset_rgb)


split_ratio = int(0.9 * x_dataset_rgb.shape[0])
x_train_rgb = x_dataset_rgb[0:split_ratio]
x_validation_rgb = x_dataset_rgb[split_ratio:]
y_train = y_dataset[0:split_ratio]
y_validation = y_dataset[split_ratio:]

# Data augmentation
#train_map = pandas.read_csv("./train_map.csv").astype(str)
#train_datagen = ImageDataGenerator(rotation_range= 10, width_shift_range=0.05, height_shift_range=0.05,
#                                   zoom_range=0.05)
#test_datagen = ImageDataGenerator()
#train_generator = train_datagen.flow_from_dataframe(dataframe=train_map, directory='./images/', class_mode='sparse',
#                                                    x_col="Id", y_col="Label", target_size=(128, 128), batch_size=32)
# # test
# Model preparation
img_shape = (img_rows, img_cols, 3)

vgg_conv = VGG16(weights='imagenet', include_top=False, input_shape=img_shape)

model = Sequential()
# Add the vgg convolutional base model
model.add(vgg_conv)
model.add(Flatten())
model.add(Dense(1024, activation='relu'))
model.add(Dense(512, activation='relu'))
model.add(Dense(num_classes, activation='softmax'))
# Compile the model
for layer in model.layers:
    layer.trainable = True

model.compile(loss='sparse_categorical_crossentropy',
              optimizer=keras.optimizers.adam(lr=0.0001),
              metrics=['accuracy'])
#
#history = model.fit_generator(train_generator,
#                              epochs=epochs,
#                              steps_per_epoch=352,
#                              verbose=1,
#                              validation_data=(x_validation_rgb, y_validation))

history = model.fit(x_dataset_rgb, y_dataset,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                   validation_data=(x_validation_rgb, y_validation))

with open('/trainHistoryDict', 'wb') as file_pi:
    pickle.dump(history.history, file_pi)

score = model.evaluate(x_dataset_rgb, y_dataset, verbose=0)
print('validation loss:', score[0])
print('validation accuracy:', score[1])

model.save('model.h5')

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'b', label='Training acc')
plt.plot(epochs, val_acc, 'r', label='Validation acc')
plt.title('Training and validation accuracy')

plt.legend()
plt.figure()
plt.plot(epochs, loss, 'b', label='Training loss')
plt.plot(epochs, val_loss, 'r', label='Validation loss')
plt.title('Training and validation loss')
plt.legend()
plt.show()